#include	"compiler.h"
#include	"np2.h"
#include	"wiikbd.h"
#include	"keystat.h"

#include <wiikeyboard/keyboard.h>

typedef struct {
	WIIKVAL	wiikey;
	UINT16	keycode;
} WIIKCNV;

#define NC 0xFF

// 101キーボード
static const WIIKCNV wiicnv101[] = {
			{WIIK_ESCAPE,		0x00},	{WIIK_1,		0x01},
			{WIIK_2,		0x02},	{WIIK_3,		0x03},
			{WIIK_4,		0x04},	{WIIK_5,		0x05},
			{WIIK_6,		0x06},	{WIIK_7,		0x07},

			{WIIK_8,		0x08},	{WIIK_9,		0x09},
			{WIIK_0,		0x0A},	{WIIK_MINUS,		0x0B},
			{WIIK_EQUALS,		0x0C},	{WIIK_BACKSLASH,	0x0D},
			{WIIK_BACKSPACE,	0x0E},	{WIIK_TAB,		0x0F},

			{WIIK_q,		0x10},	{WIIK_w,		0x11},
			{WIIK_e,		0x12},	{WIIK_r,		0x13},
			{WIIK_t,		0x14},	{WIIK_y,		0x15},
			{WIIK_u,		0x16},	{WIIK_i,		0x17},

			{WIIK_o,		0x18},	{WIIK_p,		0x19},
			{WIIK_RETURN,		0x1C},	{WIIK_a,		0x1D},
			{WIIK_s,		0x1E},	{WIIK_d,		0x1F},

			{WIIK_f,		0x20},	{WIIK_g,		0x21},
			{WIIK_h,		0x22},	{WIIK_j,		0x23},
			{WIIK_k,		0x24},	{WIIK_l,		0x25},

							{WIIK_z,		0x29},
			{WIIK_x,		0x2A},	{WIIK_c,		0x2B},
			{WIIK_v,		0x2C},	{WIIK_b,		0x2D},
			{WIIK_n,		0x2E},	{WIIK_m,		0x2F},

			{WIIK_COMMA,		0x30},	{WIIK_PERIOD,		0x31},
			{WIIK_SLASH,		0x32},
			{WIIK_SPACE,		0x34},
			{WIIK_PAGEUP,		0x36},	{WIIK_PAGEDOWN,		0x37},

			{WIIK_INSERT,		0x38},	{WIIK_DELETE,		0x39},
			{WIIK_UP,		0x3A},	{WIIK_LEFT,		0x3B},
			{WIIK_RIGHT,		0x3C},	{WIIK_DOWN,		0x3D},
			{WIIK_HOME,		0x3E},	{WIIK_END,		0x3F},

			{WIIK_KP_MINUS,		0x40},	{WIIK_KP_DIVIDE,	0x41},
			{WIIK_KP7,		0x42},	{WIIK_KP8,		0x43},
			{WIIK_KP9,		0x44},	{WIIK_KP_MULTIPLY,	0x45},
			{WIIK_KP4,		0x46},	{WIIK_KP5,		0x47},

			{WIIK_KP6,		0x48},	{WIIK_KP_PLUS,		0x49},
			{WIIK_KP1,		0x4A},	{WIIK_KP2,		0x4B},
			{WIIK_KP3,		0x4C},
			{WIIK_KP0,		0x4E},

			{WIIK_KP_PERIOD,	0x50},

			{WIIK_BREAK,		0x60},	{WIIK_PRINT,		0x61},
			{WIIK_F1,		0x62},	{WIIK_F2,		0x63},
			{WIIK_F3,		0x64},	{WIIK_F4,		0x65},
			{WIIK_F5,		0x66},	{WIIK_F6,		0x67},

			{WIIK_F7,		0x68},	{WIIK_F8,		0x69},
			{WIIK_F9,		0x6A},	{WIIK_F10,		0x6B},

			{WIIK_RSHIFT,		0x70},	{WIIK_LSHIFT,		0x70},
			{WIIK_CAPSLOCK,		0x71},
			{WIIK_RALT,		0x73},	{WIIK_LALT,		0x73},
			{WIIK_RCTRL,		0x74},	{WIIK_LCTRL,		0x74}
//			{WIIK_KP_EQUALS,	0x4D},
};


static BYTE keytbl[WIIK_LAST];

static const BYTE f12keys[] = {	0x61, 0x60, 0x4D, 0x4F};

static int oldkeysdown[WIIK_LAST];
static int keysdown[WIIK_LAST];

void wiikbd_initialize(void)
{
	int		 i;
	const WIIKCNV	*key;
	const WIIKCNV	*keyterm;

	for(i = 0; i < WIIK_LAST; i++)
		keytbl[i] = NC;
	key = wiicnv101;
	keyterm = key + (sizeof(wiicnv101) / sizeof(WIIKCNV));
	while(key < keyterm) {
		keytbl[key->wiikey] = (BYTE)key->keycode;
		key++;
	}
	for(i = 0; i < WIIK_LAST; i++)
		keysdown[i] = 0;
	KEYBOARD_Init(NULL);
}

static BYTE getf12key(void)
{
	UINT key;

	key = np2oscfg.F12KEY - 1;
	if(key < (sizeof(f12keys) / sizeof(BYTE))) {
		return f12keys[key];
	} else {
		return NC;
	}
}

void wiikbd_keycore(UINT key, BYTE mode)
{
	BYTE data;
	
	if(mode == 0)
		keysdown[key] = 1;
	else if(mode == 1)
		keysdown[key] = 0;
	else if(mode == 2)
		keysdown[key] = 2;
	if((key == WIIK_F12) && (mode == 0))
		NP2Wii_menu_display();
	else if(key < WIIK_LAST)
		data = keytbl[key];
	else
		data = NC;
	if(data != NC) {
		if(mode != 2)
			keystat_senddata(data | (mode * 0x80));
	}
}

int wiikbd_is_pressed(WIIKVAL key)
{
	return (keysdown[key] == 1) ? 1 : 0;
}

int wiikbd_is_down(WIIKVAL key)
{
	return (keysdown[key] != 0) ? 1 : 0;
}

int wiikbd_is_up(WIIKVAL key)
{
	return (keysdown[key] == 0) ? 1 : 0;
}

int* wiikbd_keydown_list()
{
	return keysdown;
}

void wiikbd_resetf12(void)
{
	UINT i;
	for(i = 0; i < (sizeof(f12keys) / sizeof(BYTE)); i++)
		keystat_forcerelease(f12keys[i]);
}
