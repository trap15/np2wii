#include	"compiler.h"
#include	"parts.h"
#include	"soundmng.h"
#include	"sound.h"
#if defined(VERMOUTH_LIB)
#include	"commng.h"
#include	"cmver.h"
#endif

#include <ogcsys.h>
#include <ogc/audio.h>
#include <ogc/cache.h>

#define SKIP_SNDMNGBUF

#ifdef SKIP_SNDMNGBUF
#define	NSNDBUF				1
#else
#define	NSNDBUF				2
#endif

#define DMABUFCNT			2

typedef struct {
	BOOL	opened;
	int	nsndbuf;
	int	samples;
#ifndef SKIP_SNDMNGBUF
	u32	*buf[NSNDBUF];
#endif
	int	ms;
} SOUNDMNG;

static	SOUNDMNG	soundmng;

static lwpq_t soundmng_waiting;
#define AUDIOSTACK 32768
static lwp_t soundmng_thread_v = LWP_THREAD_NULL;
static u8 soundmng_stack[AUDIOSTACK];
#define BUFFERSIZE (2048 + 512 + 64)
static u32 dmabufs[DMABUFCNT][BUFFERSIZE * 8] ATTRIBUTE_ALIGN(32);
static u8 bufsel = 0;
static u8 keep_athread_alive = 1;
static int dmasize = BUFFERSIZE * 4;

static inline void soundmng_copier(u8* bufout, int len)
{
	int		length;
	const s32	*src;
	
	length = min(len, (int)(soundmng.samples * 2 * sizeof(u32)));
	dmasize = length;
#ifdef SKIP_SNDMNGBUF
#define	dst	bufout
#else
#define	dst	soundmng.buf[soundmng.nsndbuf]
#endif
	src = sound_pcmlock();
	if(src != NULL) {
		memcpy(dst, src, length);
		sound_pcmunlock(src);
	}else{
		ZeroMemory(dst, length);
	}
#ifndef SKIP_SNDMNGBUF
	memcpy(bufout, dst, length);
#if NSNDBUF > 2
	soundmng.nsndbuf++;
	soundmng.nsndbuf %= NSNDBUF;
#elif NSNDBUF == 2
	soundmng.nsndbuf ^= 1;
#endif
#endif
#undef dst
}

static void* soundmng_thread(void *arg)
{
	while(1) {
		if((!soundmng.opened) && (!keep_athread_alive))
			break;
		
#if DMABUFCNT > 2
		bufsel++;
		bufsel %= DMABUFCNT;
#elif DMABUFCNT == 2
		bufsel ^= 1;
#endif
//		memset(dmabufs[bufsel], 0, sizeof(dmabufs[0]));
		
		soundmng_copier((u8*)dmabufs[bufsel], BUFFERSIZE * 4);
		LWP_ThreadSleep(soundmng_waiting);
	}
	return NULL;
}

static void soundmng_dma_callback() {

	AUDIO_StopDMA();
	DCFlushRange(dmabufs[bufsel], sizeof(dmabufs[0]));
	AUDIO_InitDMA((u32)dmabufs[bufsel], dmasize);
	AUDIO_StartDMA();
	LWP_ThreadSignal(soundmng_waiting);
}


UINT soundmng_create(UINT rate, UINT ms) {

	int i;
	u32* tmp;
	int outrate;
	switch(rate) {
		case 32000:
			outrate = AI_SAMPLERATE_32KHZ;
			break;
		case 48000:
			outrate = AI_SAMPLERATE_48KHZ;
			break;
		default:
			rate = 32000;
			outrate = AI_SAMPLERATE_32KHZ;
			break;
	}
	i = (rate * ms) / (NSNDBUF * 1000);
	soundmng.samples = 1;
	soundmng.ms = ms;
	while(soundmng.samples <= i) {
		soundmng.samples <<= 1;
	}
	soundmng.nsndbuf = 0;
#ifndef SKIP_SNDMNGBUF
	for(i = 0; i < NSNDBUF; i++) {
		tmp = memalign(32, soundmng.samples * 8);
		if(tmp == NULL) {
			return (FAILURE);
		}
		ZeroMemory(tmp, soundmng.samples * 8);
		soundmng.buf[i] = tmp;
	}
#endif
	for(i = 0; i < DMABUFCNT; i++) {
		memset(dmabufs[i], 0, sizeof(dmabufs[0]));
	}
	AUDIO_SetDSPSampleRate(outrate);			/* Set the DSP's sample rate */
	LWP_InitQueue(&soundmng_waiting);
	LWP_CreateThread(&soundmng_thread_v, soundmng_thread, NULL, soundmng_stack, AUDIOSTACK, 67);
	AUDIO_RegisterDMACallback(soundmng_dma_callback);	/* Register our DMA callback */
	soundmng_dma_callback();				/* Run it once, for good measure */
#if defined(VERMOUTH_LIB)
	cmvermouth_load(rate);
#endif
	soundmng.opened = TRUE;
	keep_athread_alive = 0;
	return soundmng.samples;
}

void soundmng_destroy(void) {

	int	i;
	u32	*tmp;
	soundmng.opened = FALSE;
	AUDIO_RegisterDMACallback(0);	/* Unload the AX DMA Callback */
	AUDIO_StopDMA();		/* Stop doing any AX DMA's */
#ifndef SKIP_SNDMNGBUF
	for(i = 0; i < NSNDBUF; i++) {
		tmp = soundmng.buf[i];
		soundmng.buf[i] = NULL;
		if(tmp)
			free(tmp);
	}
#endif
	LWP_ThreadSignal(soundmng_waiting);
	LWP_JoinThread(soundmng_thread_v, NULL);
	LWP_CloseQueue(soundmng_waiting);
	soundmng_thread_v = LWP_THREAD_NULL;
}

void soundmng_play(void) {
	AUDIO_RegisterDMACallback(soundmng_dma_callback);	/* Register our DMA callback */
	soundmng_dma_callback();				/* Run it once, for good measure */
}

void soundmng_stop(void) {
	AUDIO_RegisterDMACallback(0);
	AUDIO_StopDMA();
}


// ----

void soundmng_initialize(void) {
	AUDIO_Init(0);	/* Initialize AX Subsystem */
}

void soundmng_deinitialize(void) {

#if defined(VERMOUTH_LIB)
	cmvermouth_unload();
#endif
}

