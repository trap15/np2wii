#include	"compiler.h"
#include	"inputmng.h"
#include	"wiikbd.h"

typedef struct {
	WIIKVAL	key;
	UINT	bit;
} KEYBIND;

static const KEYBIND keybind[] = {
					{WIIK_UP,		KEY_UP},
					{WIIK_DOWN,		KEY_DOWN},
					{WIIK_LEFT,		KEY_LEFT},
					{WIIK_RIGHT,		KEY_RIGHT},
					{WIIK_RETURN,		KEY_ENTER},
					{WIIK_ESCAPE,		KEY_MENU},
					{WIIK_TAB,		KEY_SKIP}};		/* とりあえずね… */
typedef struct {
	UINT	kbs;
	KEYBIND	kb[32];
} INPMNG;

static	INPMNG	inpmng;

// ----

void inputmng_init(void) {

	INPMNG	*im;

	im = &inpmng;
	ZeroMemory(im, sizeof(INPMNG));
	im->kbs = sizeof(keybind) / sizeof(KEYBIND);
	CopyMemory(im->kb, keybind, sizeof(keybind));
}

void inputmng_keybind(short key, UINT bit) {

	INPMNG	*im;
	UINT	i;

	im = &inpmng;
	for (i=0; i<im->kbs; i++) {
		if (im->kb[i].key == key) {
			im->kb[i].bit = bit;
			return;
		}
	}
	if (im->kbs < (sizeof(im->kb) / sizeof(KEYBIND))) {
		im->kb[im->kbs].key = key;
		im->kb[im->kbs].bit = bit;
		im->kbs++;
	}
}

UINT inputmng_getkey(short key) {

	INPMNG	*im;
	KEYBIND	*kb;
	UINT	kbs;

	im = &inpmng;
	kb = im->kb;
	kbs = im->kbs;
	while(kbs--) {
		if (kb->key == key) {
			return(kb->bit);
		}
		kb++;
	}
	return(0);
}

