#include "compiler.h"

#ifndef _VIDEO_H_
#define _VIDEO_H_

int   Video_Init(int width, int height);
void  Video_Update();
void  Video_ClearScreen(u16 color);
void* Video_LockScreen();
void  Video_UnlockScreen();
int   Video_GetWidth();
int   Video_GetHeight();
void* Video_LoadImage(char* path);
void  Video_BlitImage(void* img, int srcx, int srcy, int dstx, int dsty, int dstw, int dsth);
void  Video_SetClear(int on);
void  Video_PushScreen();
void  Video_PopScreen();

#endif /* _VIDEO_H_ */
