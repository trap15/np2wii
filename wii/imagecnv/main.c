#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gd.h>

typedef   signed char		s8;
typedef   signed short		s16;
typedef   signed int		s32;
typedef   signed long long	s64;
typedef unsigned char		u8;
typedef unsigned short		u16;
typedef unsigned int		u32;
typedef unsigned long long	u64;

#define be32(x)		((((x >>  0) & 0xFF) << 24) | \
			 (((x >>  8) & 0xFF) << 16) | \
			 (((x >> 16) & 0xFF) <<  8) | \
			 (((x >> 24) & 0xFF) <<  0))

#define be16(x)		((((x >>  0) & 0xFF) <<  8) | \
			 (((x >>  8) & 0xFF) <<  0))

int ConvertBitMapToRGB565(u8* bitmapdata, u32 bitmapsize, u8** outbuf, u32 width, u32 height)
{
	s32 x, y;
	s32 x1, y1;
	u32 iv = 0;
	*outbuf = (u8*)calloc(width * height, 2);
	if(*outbuf == NULL)
		return -1;
	u8* writebuf = *outbuf;
	u32 outsz = width * height * 2;
	for(y = 0; y < height; y++) {
		for(x = 0; x < width; x++) {
			u16 newpixel = 0;
			u32 rgba = ((u32*)bitmapdata)[x + (y * width)];
			u8 r = (rgba >> 0)  & 0xFF;
			u8 g = (rgba >> 8)  & 0xFF;
			u8 b = (rgba >> 16) & 0xFF;
			newpixel = ((b >> 3) << 11) | ((g >> 2) << 5) | ((r >> 3) << 0);
			((u16*)writebuf)[iv++] = be16(newpixel);
		}
	}
	printf("Converted to 0x%08x bytes of 0x%08x bytes.\n", iv * 2, outsz);
	fflush(stdout);
	return outsz;
}

int ConvertImage(FILE* gdfp, FILE* out)
{
	u32 magic = 0x47580565;
	u32 tmp = be32(magic);
	fwrite(&tmp, 4, 1, out);

	gdImagePtr im;
	im = gdImageCreateFromPng(gdfp);
	gdImageAlphaBlending(im, 0);
	gdImageSaveAlpha(im, 1);
	u32 width = gdImageSX(im);
	tmp = be32(width);
	fwrite(&tmp, 4, 1, out);
	u32 height = gdImageSY(im);
	tmp = be32(height);
	fwrite(&tmp, 4, 1, out);
	u32 bitmapsize = width * height * 4;
	u8* bitmapdata = (u8*)malloc(width * height * 4);
	memset(bitmapdata, 0, width * height * 4);
	u8* outbuf;
	u32 x, y;
	for(y = 0; y < height; y++) {
		for(x = 0; x < width; x++) {
			u32 p;
			if(x >= width || y >= height)
				p = 0;
			else
				p = gdImageGetPixel(im, x, y);
			u8 a = 254 - 2 * ((u8)gdImageAlpha(im, p));
			if(a == 254) a++;
			u8 r = (u8)gdImageRed(im, p);
			u8 g = (u8)gdImageGreen(im, p);
			u8 b = (u8)gdImageBlue(im, p);
			u32 rgba = (r << 8) | (g << 16) | (b << 24) | (a << 0);
			rgba = be32(rgba);
			((u32*)bitmapdata)[x + (y * width)] = rgba;
		}
	}
	int ret = ConvertBitMapToRGB565(bitmapdata, bitmapsize, &outbuf, width, height);
	if(ret == -1) {
		printf("Error converting file.\n");
		return -1;
	}
	fwrite(outbuf, width * height, 2, out);
	return 0;
}	

int main(int argc, char *argv[])
{
	printf("IMG->RGB565 (C)2009 SquidMan (Alex Marshall)\nAll rights reserved.\n");
	if(argc < 3) {
		printf("Invalid arguments. Usage:\n\t%s in.png out.rgb\n", argv[0]);
		exit(1);
	}
	FILE* in  = fopen(argv[1], "rb");
	FILE* out = fopen(argv[2], "wb");
	if(in == NULL) {
		printf("Unable to open %s\n", argv[1]);
		exit(2);
	}
	if(out == NULL) {
		printf("Unable to open %s\n", argv[2]);
		exit(3);
	}
	int ret = ConvertImage(in, out);
	if(ret == -1) {
		printf("Unable to convert.\n");
		exit(4);
	}
	fclose(in);
	fclose(out);
	return 0;
}

