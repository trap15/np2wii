#include	"compiler.h"
// #include	<signal.h>
#include	"inputmng.h"
#include	"taskmng.h"
#include	"wiikbd.h"
#include	"vramhdl.h"
#include	"menubase.h"
#include	"sysmenu.h"

#include "wii.h"

BOOL	task_avail;

void sighandler(int signo)
{
	(void)signo;
	task_avail = FALSE;
}

void taskmng_initialize(void)
{
	task_avail = TRUE;
}

void taskmng_exit(void)
{
	task_avail = FALSE;
}

void taskmng_rol_core()
{
	keyboard_event ke;
	s32 stat;
	
	NP2Wii_control_handle();
	stat = KEYBOARD_GetEvent(&ke);
	if(!stat)
		return;
	switch(ke.type) {
		case KEYBOARD_RELEASED:
			wiikbd_keyup(ke.keycode);
			break;
		case KEYBOARD_PRESSED:
			wiikbd_keydown(ke.keycode);
			break;
	}
}

void taskmng_rol(void)
{
	if(!task_avail) {
		return;
	}
	taskmng_rol_core();
}

BOOL taskmng_sleep(UINT32 tick)
{
	UINT32	base;

	base = GETTICK();
	while((task_avail) && ((GETTICK() - base) < tick)) {
		taskmng_rol();
		usleep(960);
	}
	return(task_avail);
}

