/*****************************************************************************
 *  /wii/wii.c                                                               *
 *  Wii specific code source.                                                *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *
 *  Copyright (C)2009 SquidMan (Alex Marshall)       <SquidMan72@gmail.com>  *
 *  Protected under the GNU GPLv2                                            *
 *****************************************************************************/

#include <gctypes.h>
#include <stdio.h>
#include <dirent.h>

#include <wiiuse/wpad.h>
#include <elm.h>

#include "compiler.h"
#include "dosio.h"
#include "scrnmng.h"
#include "diskdrv.h"
#include "taskmng.h"
#include "wiikbd.h"
#include "wii.h"
#include "x68kprint.h"
#include "np2.h"
#include "pccore.h"

#define KEYUNUSED		0
#define KEYMENU			WIIK_F12

#define WOFF			(20)
#define HOFF			(20)
#define MAX_ENTRIES		(16)
#define FONTSIZE		(16)
#define VALUE_OFF		(40)
#define SWITCH_OFF		(18)

#define FILETYPE_UNKNOWN	0
#define FILETYPE_HDD_HDI	1
#define FILETYPE_FDD_D88	2
#define FILETYPE_FDD_MTR	3
#define FILETYPE_FDD_XDF	4
#define FILETYPE_COUNT		5

#define BASETYPE_UNKNOWN	0
#define BASETYPE_HDD		1
#define BASETYPE_FDD		2
#define BASETYPE_COUNT		3

static u16 wiimote_key_map[3][0x1C] = {
/********************************** WIIMOTE **********************************/
	{ /* DOWN  */	WIIK_RIGHT,
	  /* UP    */	WIIK_LEFT,
	  /* LEFT  */	WIIK_DOWN,
	  /* RIGHT */	WIIK_UP,
	  /* PLUS  */	WIIK_ESCAPE,
	  /* HOME  */	KEYMENU,
	  /* MINUS */	WIIK_ESCAPE,
	  /* A     */	WIIK_RETURN,
	  /* B     */	WIIK_ESCAPE,
	  /* 1     */	WIIK_x,
	  /* 2     */	WIIK_z,
/* NUNCHUK   C     */	KEYUNUSED,
/* NUNCHUK   Z     */	KEYUNUSED,
/* CLASSIC   RIGHT */	KEYUNUSED,
/* CLASSIC   DOWN  */	KEYUNUSED,
/* CLASSIC   FUL_L */	KEYUNUSED,
/* CLASSIC   MINUS */	KEYUNUSED,
/* CLASSIC   HOME  */	KEYUNUSED,
/* CLASSIC   PLUS  */	KEYUNUSED,
/* CLASSIC   FUL_R */	KEYUNUSED,
/* CLASSIC   ZL    */	KEYUNUSED,
/* CLASSIC   B     */	KEYUNUSED,
/* CLASSIC   Y     */	KEYUNUSED,
/* CLASSIC   A     */	KEYUNUSED,
/* CLASSIC   X     */	KEYUNUSED,
/* CLASSIC   ZR    */	KEYUNUSED,
/* CLASSIC   LEFT  */	KEYUNUSED,
/* CLASSIC   UP    */	KEYUNUSED,
	},
/********************************** NUNCHUK **********************************/
	{ /* DOWN  */	WIIK_DOWN,
	  /* UP    */	WIIK_UP,
	  /* LEFT  */	WIIK_LEFT,
	  /* RIGHT */	WIIK_RIGHT,
	  /* PLUS  */	WIIK_ESCAPE,
	  /* HOME  */	KEYMENU,
	  /* MINUS */	WIIK_ESCAPE,
	  /* A     */	WIIK_RETURN,
	  /* B     */	WIIK_ESCAPE,
	  /* 1     */	WIIK_x,
	  /* 2     */	WIIK_z,
/* NUNCHUK   C     */	WIIK_z,
/* NUNCHUK   Z     */	WIIK_x,
/* CLASSIC   RIGHT */	KEYUNUSED,
/* CLASSIC   DOWN  */	KEYUNUSED,
/* CLASSIC   FUL_L */	KEYUNUSED,
/* CLASSIC   MINUS */	KEYUNUSED,
/* CLASSIC   HOME  */	KEYUNUSED,
/* CLASSIC   PLUS  */	KEYUNUSED,
/* CLASSIC   FUL_R */	KEYUNUSED,
/* CLASSIC   ZL    */	KEYUNUSED,
/* CLASSIC   B     */	KEYUNUSED,
/* CLASSIC   Y     */	KEYUNUSED,
/* CLASSIC   A     */	KEYUNUSED,
/* CLASSIC   X     */	KEYUNUSED,
/* CLASSIC   ZR    */	KEYUNUSED,
/* CLASSIC   LEFT  */	KEYUNUSED,
/* CLASSIC   UP    */	KEYUNUSED,
	},
/**************************** CLASSIC CONTROLLER *****************************/
	{ /* DOWN  */	KEYUNUSED,
	  /* UP    */	KEYUNUSED,
	  /* LEFT  */	KEYUNUSED,
	  /* RIGHT */	KEYUNUSED,
	  /* PLUS  */	KEYUNUSED,
	  /* HOME  */	KEYMENU,
	  /* MINUS */	KEYUNUSED,
	  /* A     */	KEYUNUSED,
	  /* B     */	KEYUNUSED,
	  /* 1     */	KEYUNUSED,
	  /* 2     */	KEYUNUSED,
/* NUNCHUK   C     */	KEYUNUSED,
/* NUNCHUK   Z     */	KEYUNUSED,
/* CLASSIC   RIGHT */	WIIK_RIGHT,
/* CLASSIC   DOWN  */	WIIK_DOWN,
/* CLASSIC   FUL_L */	WIIK_LSHIFT,
/* CLASSIC   MINUS */	WIIK_ESCAPE,
/* CLASSIC   HOME  */	KEYMENU,
/* CLASSIC   PLUS  */	WIIK_ESCAPE,
/* CLASSIC   FUL_R */	WIIK_RSHIFT,
/* CLASSIC   ZL    */	WIIK_LSHIFT,
/* CLASSIC   B     */	WIIK_x,
/* CLASSIC   Y     */	WIIK_z,
/* CLASSIC   A     */	WIIK_z,
/* CLASSIC   X     */	WIIK_x,
/* CLASSIC   ZR    */	WIIK_RSHIFT,
/* CLASSIC   LEFT  */	WIIK_LEFT,
/* CLASSIC   UP    */	WIIK_UP,
	},
};

static u32 oldbtn;

typedef struct {
	char	*name;
	char	*path;
	int	type;
	int	basetype;
	int	isdir;
} gamelistentry;

typedef struct {
	char	*hdd0;
	char	*hdd1;
	char	*fdd0;
	char	*fdd1;
	char	*hdd0n;
	char	*hdd1n;
	char	*fdd0n;
	char	*fdd1n;
} load_entry;

load_entry selected = { NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL };
int do_wiimenu_exit = 0;
static int init = 0;

#define NP2WII_BTNCHK(a) {				\
	if((btn & a) && (!(oldbtn & a))) {		\
		z = NP2Wii_control_lookup(a, ext.type);	\
		wiikbd_keydown(z);			\
		oldbtn |= a;				\
	}else if(btn & a){				\
		z = NP2Wii_control_lookup(a, ext.type);	\
		wiikbd_keypressed(z);			\
	}						\
	if((!(btn & a)) && (oldbtn & a)) {		\
		z = NP2Wii_control_lookup(a, ext.type);	\
		wiikbd_keyup(z);			\
		oldbtn &= ~a;				\
	}						\
}

#define MENU_UP_DOWN_HANDLERS(cnt)			\
if(wiikbd_is_pressed(WIIK_DOWN)) {			\
	cur_off++;					\
	position++;					\
							\
	redraw = 1;					\
							\
	if(position >= cnt) {				\
		position = 0;				\
		cur_off = 0;				\
		redraw = 1;				\
	}						\
	if(cur_off >= MAX_ENTRIES) {			\
		cur_off = MAX_ENTRIES - 1;		\
		redraw = 1;				\
	}						\
}else if(wiikbd_is_pressed(WIIK_UP)) {			\
	cur_off--;					\
	position--;					\
							\
	redraw = 1;					\
							\
	if(cur_off < 0) {				\
		cur_off = 0;				\
		redraw = 1;				\
	}						\
	if(position < 0) {				\
		position = cnt - 1;			\
		redraw = 1;				\
	}						\
	if(position < 0)				\
		position = 0;				\
}

#define GENERIC_INCREASE(x, y, z)		\
	case x:					\
		y++;				\
		y %= z;				\
		break;

#define GENERIC_DECREASE(x, y, z)		\
	case x:					\
		y--;				\
		if(y < 0)			\
			y = z - 1;		\
		if(y > z)			\
			y = z - 1;		\
		break;


int wii_running_now = 1;

void NP2Wii_shutdown_wiimote(s32 chan)
{
	taskmng_exit();
	wii_running_now = 0;
}

void NP2Wii_shutdown_power()
{
	taskmng_exit();
	wii_running_now = 0;
}

void NP2Wii_screenshot()
{
	SCRNSURF *scrn = (SCRNSURF*)scrnmng_surflock();
	FILEH handle = file_open("sd:/scrn.bmp");
	file_write(handle, scrn->ptr, scrn->width * scrn->height * scrn->xalign);
	file_close(handle);
	scrnmng_surfunlock(scrn);
}

static inline void load_disk(int type, int idx, char* fn)
{
	switch(type) {
		case 0:
			diskdrv_sethdd(idx, fn);
			break;
		case 1:
			diskdrv_setfdd(idx, fn, 0);
			break;
	}
}

static inline void load_hdd(int idx, char *fn)
{
	load_disk(0, idx, fn);
}

static inline void load_fdd(int idx, char *fn)
{
	load_disk(1, idx, fn);
}

static inline void load_hdd0(char *fn)
{
	load_hdd(0, fn);
}

static inline void load_fdd0(char *fn)
{
	load_fdd(0, fn);
}

static inline void load_hdd1(char *fn)
{
	load_hdd(1, fn);
}

static inline void load_fdd1(char *fn)
{
	load_fdd(1, fn);
}

static void PrintStringCore(char string[], int x, int y)
{
	int w, h;
	X68000Printer_GetSize(string, &w, &h);
	if(x == -1) {
		x = (640 - w) / 2;
	}
	if(y == -1) {
		y = (480 - h) / 2;
	}
	X68000Printer_PrintLocation(string, x, y);
}

static void PrintString(int x, int y, char string[], ...)
{
	char buffer[1024];
	va_list args;
	va_start(args, string);
	vsprintf(buffer, string, args);
	PrintStringCore(buffer, x, y);
	va_end(args);
}

void MakeLowercase(char *path)
{
	int i;
	int len = strlen(path);
	for(i = 0; i < len; i++) {
		path[i] = tolower(path[i]);
	}
}

void MakeUppercase(char *path)
{
	int i;
	int len = strlen(path);
	for(i = 0; i < len; i++) {
		path[i] = toupper(path[i]);
	}
}

static int GetBasetype(int type)
{
	int ret = BASETYPE_UNKNOWN;
	switch(type) {
		case FILETYPE_FDD_D88:
		case FILETYPE_FDD_MTR:
		case FILETYPE_FDD_XDF:
			ret = BASETYPE_FDD;
			break;
		case FILETYPE_HDD_HDI:
			ret = BASETYPE_HDD;
			break;
		default:
			break;
	}
	return ret;
}

static int GetFiletype(char *path)
{
	int ret = FILETYPE_UNKNOWN;
	char *ptr = strrchr(path, '.');
	if(ptr == NULL)
		return ret;
	if(strcmp(ptr, ".hdi") == 0)
		ret = FILETYPE_HDD_HDI;
	else if(strcmp(ptr, ".d88") == 0)
		ret = FILETYPE_FDD_D88;
	else if(strcmp(ptr, ".mtr") == 0)
		ret = FILETYPE_FDD_MTR;
	else if(strcmp(ptr, ".xdf") == 0)
		ret = FILETYPE_FDD_XDF;
	return ret;
}

int sortgames_cb(const void *f1, const void *f2)
{
        /* Special case for implicit directories */
        if(((((gamelistentry*)f1)->path[0] == '.') && (((gamelistentry*)f1)->path[1] != '_')) ||	\
	   ((((gamelistentry*)f2)->path[0] == '.') && (((gamelistentry*)f2)->path[1] != '_')))
        {
                if(strcmp(((gamelistentry*)f1)->path, ".") == 0) { return -1; }
                if(strcmp(((gamelistentry*)f2)->path, ".") == 0) { return 1; }
                if(strcmp(((gamelistentry*)f1)->path, "..") == 0) { return -1; }
                if(strcmp(((gamelistentry*)f2)->path, "..") == 0) { return 1; }
        }
	
        /* If one is a file and one is a directory the directory is first. */
        if(((gamelistentry*)f1)->isdir && !(((gamelistentry*)f2)->isdir)) return -1;
        if(!(((gamelistentry*)f1)->isdir) && ((gamelistentry*)f2)->isdir) return 1;
	
        return stricmp(((gamelistentry*)f1)->path, ((gamelistentry*)f2)->path);
}

int* wiimenu_buildgamelist(int device, char* root, int max, \
			   gamelistentry *gamesunk, gamelistentry *gamesfdd, gamelistentry *gameshdd)
{
	int i = 0;
	int hoff = HOFF;
	int h = 16;
	int *sizes = calloc(sizeof(int), BASETYPE_COUNT);
	gamelistentry *games;
	memset(sizes, 0, BASETYPE_COUNT * sizeof(int));
	DIR_ITER *dirIter = NULL;
	char dir[MAXPATHLEN];
	ELM_GetExactPath(device, root, dir);
	
	dirIter = diropen(dir);
	if(dirIter == NULL) {
		exit(0);
	}
	char filename[MAXPATHLEN];
	char* origname;
        struct stat filestat;
	int res;
	int type, basetype;
	Video_SetClear(0);
        for(i = 0; i < max; i++) {
		res = dirnext(dirIter, filename, &filestat);
		if(res != 0) {
			break;
		}
		
		origname = strdup(filename);
		if(origname == NULL) {
			printf("Out of memory.\n");
			exit(1);
		}
		MakeLowercase(filename);
		type = GetFiletype(filename);
		basetype = GetBasetype(type);

		switch(basetype) {
			case BASETYPE_UNKNOWN:
				games = gamesunk;
				break;
			case BASETYPE_FDD:
				games = gamesfdd;
				break;
			case BASETYPE_HDD:
				games = gameshdd;
				break;
		}
		games[sizes[basetype]].path = calloc(strlen(filename) + strlen(dir) + 1, 1);
		sprintf(games[sizes[basetype]].path, "%s/%s", dir, filename);
		games[sizes[basetype]].isdir = (filestat.st_mode & _IFDIR) == 0 ? 0 : 1;
		games[sizes[basetype]].type = type;
		games[sizes[basetype]].basetype = basetype;
		
		if(games[sizes[basetype]].isdir) {
			if(strcmp(filename, "..") == 0)
				sizes[basetype]--;
			else if(strcmp(filename, ".") == 0)
				sizes[basetype]--;
			else {
				games[sizes[basetype]].name = origname;
			}
		}else{
			games[sizes[basetype]].name = origname;
		}
		sizes[basetype]++;
	}
	Video_SetClear(1);
	// Sort the file lists
        if(sizes[BASETYPE_UNKNOWN] >= 0)
		qsort(gamesunk, sizes[BASETYPE_UNKNOWN], sizeof(gamelistentry), sortgames_cb);
        if(sizes[BASETYPE_HDD] >= 0)
		qsort(gameshdd, sizes[BASETYPE_HDD], sizeof(gamelistentry), sortgames_cb);
        if(sizes[BASETYPE_FDD] >= 0)
		qsort(gamesfdd, sizes[BASETYPE_FDD], sizeof(gamelistentry), sortgames_cb);
	
	dirclose(dirIter);	// close directory
	return sizes;
}

load_entry wiimenu_choosefromlist(int device, char *root, gamelistentry *gamesunk, gamelistentry *gamesfdd, gamelistentry *gameshdd, \
				  int* listcnt, load_entry* ent)
{
	int go = 1;
	int position = 0;
	int cur_off = 0;
	int i = 0;
	int redraw = 1;
	int devnum = 0;
	int devtype = BASETYPE_HDD;
	char *devices[] = { "HDD0", "HDD1", "FDD0", "FDD1" };
	int sel[] = { -1, -1, -1, -1 };
	VIDEO_Flush();
	VIDEO_WaitVSync();
	int w, h;
	w = X68000Printer_GetWidth("A");
	h = 16;
	
	while(go) {
		taskmng_rol_core();
		if(wiikbd_is_pressed(WIIK_ESCAPE)) {
			go = 0;
		}
		if(wiikbd_is_pressed(WIIK_x)) {
			devnum++;
			devnum %= 4;
			switch(devnum) {
				case 0:
				case 1:
					devtype = BASETYPE_HDD;
					break;
				case 2:
				case 3:
					devtype = BASETYPE_FDD;
					break;
			}
			position = 0;
			cur_off = 0;
			i = 0;
			redraw = 1;
		}
		if(wiikbd_is_pressed(WIIK_RETURN)) {
			switch(devnum) {
				case 0:
					if(ent->hdd0 == NULL) {
						ent->hdd0  = calloc(256, 1);
					}else{
						memset(ent->hdd0, 0, 256);
					}
					if(ent->hdd0n != NULL) {
						_MFREE(ent->hdd0n);
						ent->hdd0n = NULL;
					}
					sprintf(ent->hdd0, "%s", gameshdd[position].path);
					ent->hdd0n = strdup(gameshdd[position].name);
					break;
				case 1:
					if(ent->hdd1 == NULL) {
						ent->hdd1  = calloc(256, 1);
					}else{
						memset(ent->hdd1, 0, 256);
					}
					if(ent->hdd1n != NULL) {
						_MFREE(ent->hdd1n);
						ent->hdd1n = NULL;
					}
					sprintf(ent->hdd1, "%s", gameshdd[position].path);
					ent->hdd1n = strdup(gameshdd[position].name);
					break;
				case 2:
					if(ent->fdd0 == NULL) {
						ent->fdd0  = calloc(256, 1);
					}else{
						memset(ent->fdd0, 0, 256);
					}
					if(ent->fdd0n != NULL) {
						_MFREE(ent->fdd0n);
						ent->fdd0n = NULL;
					}
					sprintf(ent->fdd0, "%s", gamesfdd[position].path);
					ent->fdd0n = strdup(gamesfdd[position].name);
					break;
				case 3:
					if(ent->fdd1 == NULL) {
						ent->fdd1  = calloc(256, 1);
					}else{
						memset(ent->fdd1, 0, 256);
					}
					if(ent->fdd1n != NULL) {
						_MFREE(ent->fdd1n);
						ent->fdd1n = NULL;
					}
					sprintf(ent->fdd1, "%s", gamesfdd[position].path);
					ent->fdd1n = strdup(gamesfdd[position].name);
					break;
			}
			sel[devnum] = position;
			redraw = 1;
		}
		if(wiikbd_is_pressed(WIIK_z)) {
			switch(devnum) {
				case 0:
					_MFREE(ent->hdd0);
					_MFREE(ent->hdd0n);
					break;
				case 1:
					_MFREE(ent->hdd1);
					_MFREE(ent->hdd1n);
					break;
				case 2:
					_MFREE(ent->fdd0);
					_MFREE(ent->fdd0n);
					break;
				case 3:
					_MFREE(ent->fdd1);
					_MFREE(ent->fdd1n);
					break;
			}
			sel[devnum] = -1;
			redraw = 1;
		}
		MENU_UP_DOWN_HANDLERS(listcnt[devtype])
		int hoff = HOFF;
		PrintString(-1, hoff, "Load %s.", devices[devnum]);
		hoff += h;
		PrintString(-1, hoff, "Currently loaded: %s",	(devnum == 0) ? (((ent->hdd0n != NULL) && (ent->hdd0 != NULL)) ? ent->hdd0n : "None") : (\
								(devnum == 1) ? (((ent->hdd1n != NULL) && (ent->hdd1 != NULL)) ? ent->hdd1n : "None") : (\
								(devnum == 2) ? (((ent->fdd0n != NULL) && (ent->fdd0 != NULL)) ? ent->fdd0n : "None") : (\
								(devnum == 3) ? (((ent->fdd1n != NULL) && (ent->fdd1 != NULL)) ? ent->fdd1n : "None") : "None"))));
		hoff += h;
		PrintString(-1, hoff, "Press 1 to change device. Press 2 to eject.");
		hoff += h * 2;
		i = 0;
		// position == real  location
		// cur_off  == arrow location
		i = position - (MAX_ENTRIES - 1);
		if(i < 0)
			i = 0;
		if(i > (MAX_ENTRIES - 1)) {
			while(i > (MAX_ENTRIES - 1)) {
				i -= MAX_ENTRIES;
			}
		}
		int x;
		int bak = -1;
		devtype %= BASETYPE_COUNT;
		for(x = 0; (i < listcnt[devtype]) && (x < MAX_ENTRIES); i++, x++) {
			switch(devtype) {
				case BASETYPE_FDD:
					PrintString(WOFF, hoff, "  %s\n", gamesfdd[i].name);
					break;
				case BASETYPE_HDD:
					PrintString(WOFF, hoff, "  %s\n", gameshdd[i].name);
					break;
				default:
					break;
			}
			if(position == i) {
				PrintString(WOFF, hoff, "*");
				bak = x;
			}
			hoff += h;
		}
		redraw = 0;
		Video_Update();
		if(!wii_running_now)
			go = 0;
	}
	return *ent;
}

void wiimenu_loadgame(int device, char *source)
{
	gamelistentry gamesunk[20];
	gamelistentry gamesfdd[20];
	gamelistentry gameshdd[20];
	int loaded = 0;
	int h = 16;
	int hoff = HOFF;
	int* listcnt = wiimenu_buildgamelist(device, source, 20, gamesunk, gamesfdd, gameshdd);
	if(listcnt == NULL) {
		exit(0);
	}
	if((listcnt[1] == 0) && (listcnt[2] == 0)) {
		PrintString(-1, hoff, "No ROMs found in %s:%s... exiting.", ELM_GetDeviceName(device), source);
		Video_Update();
		sleep(3);
		exit(0);
	}
	while(!loaded) {
		h = 16;
		wiimenu_choosefromlist(device, source, gamesunk, gamesfdd, gameshdd, listcnt, &selected);
		Video_SetClear(0);
		if(selected.hdd0 != NULL) {
			PrintString(-1, hoff, "Loading HDD0 from [%s]", selected.hdd0);
			Video_Update();
			sleep(2);
			hoff += h;
			load_hdd0(selected.hdd0);
			loaded = 1;
		}
		if(selected.hdd1 != NULL) {
			PrintString(-1, hoff, "Loading HDD1 from [%s]", selected.hdd1);
			Video_Update();
			sleep(5);
			hoff += h;
			load_hdd1(selected.hdd1);
			loaded = 1;
		}
		if(selected.fdd0 != NULL) {
			PrintString(-1, hoff, "Loading FDD0 from [%s]", selected.fdd0);
			Video_Update();
			sleep(5);
			hoff += h;
			load_fdd0(selected.fdd0);
			loaded = 1;
		}
		if(selected.fdd1 != NULL) {
			PrintString(-1, hoff, "Loading FDD1 from [%s]", selected.fdd1);
			Video_Update();
			sleep(5);
			hoff += h;
			loaded = 1;
			load_fdd1(selected.fdd1);
		}
		Video_SetClear(1);
		if(!wii_running_now)
			loaded = 1;
	}
	if(wii_running_now)
		diskdrv_hddbind();
	_MFREE(listcnt);
	_MFREE(selected.hdd0);	/* Oh god, please forgive me for this failure ;( */
	_MFREE(selected.hdd1);	/* TODO: Find out why we crash on choosing shit in the menu if we */
	_MFREE(selected.fdd0);	/*       Don't do this free'ing stuff. */
	_MFREE(selected.fdd1);
	_MFREE(selected.hdd0n);
	_MFREE(selected.hdd1n);
	_MFREE(selected.fdd0n);
	_MFREE(selected.fdd1n);
}

static void DisplayFileList(int device, char* source)
{
	wiimenu_loadgame(device, source);
}

static void PrintSwitches(unsigned char bits, int col, int hoff)
{
	int i;
	int woff = WOFF + (8 * SWITCH_OFF);
	for(i = 0; i < 8; i++) {
		if(col == i)
			PrintString(woff, hoff, ">");
		else
			PrintString(woff, hoff, " ");
		woff += 8;
		PrintString(woff, hoff, "%s ", (bits & (1 << i)) ? "#" : " "); woff += 16;
	}
}

/* Array 1:
 * ?
 * ?
 * Plasma Display
 * Boot Order (default: HDD -> Floppy)
 * Serial Mode
 * '' ''
 * ?
 * Graphic Mode
 */
/* Array 2:
 * ?
 * Terminal Mode
 * ?
 * Line Count in Text Mode
 * Memory Switch
 * ? Disk?
 * ?
 * GDC 5MHz
 */
/* Array 3:
 * ?
 * ?
 * ?
 * Floppy Motor ?
 * DMA Clock
 * ?
 * ?
 * CPU Mode
 */

static void dip_switches()
{
	int go = 1;
	int position = 0;
	int col = 0;
	int cur_off = 0;
	int i = 0;
	int redraw = 1;
	int listcnt = 3;
	VIDEO_Flush();
	VIDEO_WaitVSync();
	while(go) {
		taskmng_rol_core();
		if(wiikbd_is_pressed(WIIK_ESCAPE)) {
			go = 0;
		}
		if(wiikbd_is_pressed(WIIK_RETURN)) {
			np2cfg.dipsw[position] ^= 1 << col;
			redraw = 1;
		}
		if(wiikbd_is_pressed(WIIK_LEFT)) {
			switch(listcnt) {
				GENERIC_DECREASE(3, col, 8)
			}
		}
		if(wiikbd_is_pressed(WIIK_RIGHT)) {
			switch(listcnt) {
				GENERIC_INCREASE(3, col, 8)
			}
		}
		MENU_UP_DOWN_HANDLERS(3)
		int hoff = HOFF;
		PrintString(WOFF, hoff, "  DIP Switches:"); hoff += 16;
		PrintString(WOFF, hoff, "                   1  2  3  4  5  6  7  8  "); hoff += 16;
		PrintString(WOFF, hoff, "  Switch array 1: ");
		PrintSwitches(np2cfg.dipsw[0], (position == 0) ? col : 9001, hoff); hoff += 16;
		PrintString(WOFF, hoff, "  Switch array 2: ");
		PrintSwitches(np2cfg.dipsw[1], (position == 1) ? col : 9001, hoff); hoff += 16;
		PrintString(WOFF, hoff, "  Switch array 3: ");
		PrintSwitches(np2cfg.dipsw[2], (position == 2) ? col : 9001, hoff); hoff += 16;
		
		i = 0;
		// position == real  location
		// cur_off  == arrow location
		i = position - (MAX_ENTRIES - 1);
		if(i < 0)
			i = 0;
		if(i > (MAX_ENTRIES - 1)) {
			while(i > (MAX_ENTRIES - 1)) {
				i -= MAX_ENTRIES;
			}
		}
		int x;
		int bak = -1;
		hoff = HOFF + (16 * 2);
		for(x = 0; (i < listcnt) && (x < MAX_ENTRIES); i++, x++) {
			if(position == i) {
				PrintString(WOFF, hoff, "*");
				bak = x;
			}
			hoff += 16;
		}
		redraw = 0;
		Video_Update();
		if(!wii_running_now)
			go = 0;
	}
}

static int advance_options(int position)
{
	switch(position) {
		GENERIC_INCREASE(0, np2oscfg.DRAW_SKIP, 5)
		GENERIC_INCREASE(1, np2oscfg.NOWAIT, 2)
		GENERIC_INCREASE(2, np2cfg.KEY_MODE, 4)
		case 3:
			// sound_config();
			break;
		case 4:
			np2cfg.EXTMEM += np2cfg.EXTMEM + 1;
			np2cfg.EXTMEM %= 15;
			break;
		case 5:
			rs232c_midipanic();
			mpu98ii_midipanic();
			break;
		case 6:
			dip_switches();
			break;
		case 7:
			// bios_settings();
			break;
		case 8:
			return 1;
	}
	return 0;
}	

static int decrement_options(int position)
{
	switch(position) {
			GENERIC_DECREASE(0, np2oscfg.DRAW_SKIP, 5)
			GENERIC_DECREASE(1, np2oscfg.NOWAIT, 2)
			GENERIC_DECREASE(2, np2cfg.KEY_MODE, 4)
		case 3:
			// sound_config();
			break;
		case 4:
			if(np2cfg.EXTMEM != 0)
				np2cfg.EXTMEM -= (np2cfg.EXTMEM + 1) / 2;
			else
				np2cfg.EXTMEM = 7;
			break;
		case 5:
			rs232c_midipanic();
			mpu98ii_midipanic();
			break;
		case 6:
			dip_switches();
			break;
		case 7:
			// bios_settings();
			break;
		case 8:
			return 1;
	}
	return 0;
}	

static void EmulationOptions()
{
	int go = 1;
	int position = 0;
	int cur_off = 0;
	int i = 0;
	int redraw = 1;
	int listcnt = 9;
	VIDEO_Flush();
	VIDEO_WaitVSync();
	while(go) {
		taskmng_rol_core();
		if(wiikbd_is_pressed(WIIK_ESCAPE)) {
			go = 0;
		}
		if(wiikbd_is_pressed(WIIK_RETURN) || wiikbd_is_pressed(WIIK_RIGHT)) {
			if(advance_options(position))
				go = 0;
			redraw = 1;
		}
		if(wiikbd_is_pressed(WIIK_LEFT)) {
			if(decrement_options(position))
				go = 0;
			redraw = 1;
		}
		MENU_UP_DOWN_HANDLERS(listcnt)
		int hoff = HOFF;
		PrintString(WOFF, hoff, "  Frameskip:");
		if(np2oscfg.DRAW_SKIP != 0) {
			PrintString(WOFF + (8 * VALUE_OFF), hoff, " %d", np2oscfg.DRAW_SKIP - 1);
		}else PrintString(WOFF + (8 * VALUE_OFF), hoff, " AUTO");
		hoff += 16;
		PrintString(WOFF, hoff, "  Framerate Limit:");
		PrintString(WOFF + (8 * VALUE_OFF), hoff, " %s", (np2oscfg.NOWAIT == 0) ?	"ON" : \
												"OFF");
		hoff += 16;
		PrintString(WOFF, hoff, "  Key Mode:");
		PrintString(WOFF + (8 * VALUE_OFF), hoff, " %s",(np2cfg.KEY_MODE == 0) ?	"KEYS" : (\
								(np2cfg.KEY_MODE == 1) ?	"JOY1" : (\
								(np2cfg.KEY_MODE == 2) ?	"JOY2" : (\
								(np2cfg.KEY_MODE == 3) ?	"MOUSE" : \
												"UNK"))));
		hoff += 16;
		PrintString(WOFF, hoff, "  Sound Config [DISABLED]"); hoff += 16;
		PrintString(WOFF, hoff, "  Memory Size:");
		PrintString(WOFF + (8 * VALUE_OFF), hoff, " %s",(np2cfg.EXTMEM == 0) ?	"640K"		: (\
								(np2cfg.EXTMEM == 1) ?	"640K + 1MB"	: (\
								(np2cfg.EXTMEM == 3) ?	"640K + 3MB"	: (\
								(np2cfg.EXTMEM == 7) ?	"640K + 7MB"	: \
											"UNK"))));
		hoff += 16;
		PrintString(WOFF, hoff, "  MIDI Panic"); hoff += 16;
		PrintString(WOFF, hoff, "  DIP Switches"); hoff += 16;
		PrintString(WOFF, hoff, "  BIOS Settings [DISABLED]"); hoff += 16;
		PrintString(WOFF, hoff, "  Return"); hoff += 16;
		
		i = 0;
		// position == real  location
		// cur_off  == arrow location
		i = position - (MAX_ENTRIES - 1);
		if(i < 0)
			i = 0;
		if(i > (MAX_ENTRIES - 1)) {
			while(i > (MAX_ENTRIES - 1)) {
				i -= MAX_ENTRIES;
			}
		}
		int x;
		int bak = -1;
		hoff = HOFF;
		for(x = 0; (i < listcnt) && (x < MAX_ENTRIES); i++, x++) {
			if(position == i) {
				PrintString(WOFF, hoff, "*");
				bak = x;
			}
			hoff += 16;
		}
		redraw = 0;
		Video_Update();
		if(!wii_running_now)
			go = 0;
	}
}

static void ReturnTo(int loc)
{
	switch(loc) {
		case 0:
			NP2Wii_shutdown_power();
			break;
		case 1:
			NP2Wii_shutdown_power();
			do_wiimenu_exit = 1;
			break;
	}
}

static void DisplayMenu()
{
	int go = 1;
	int position = 0;
	int rposition = 0;
	int cur_off = 0;
	int i = 0;
	int redraw = 1;
	int listcnt = 5 + (init * 2);
	VIDEO_Flush();
	VIDEO_WaitVSync();
	while(go) {
		taskmng_rol_core();
		if(wiikbd_is_pressed(WIIK_ESCAPE)) {
			if(init)
				go = 0;
		}
		if(wiikbd_is_pressed(WIIK_RETURN)) {
			if(!init)
				rposition = position + 2;
			else
				rposition = position;
			switch(rposition) {
				case 0:
					go = 0;
					break;
				case 1:
					pccore_cfgupdate();
					pccore_reset();
					go = 0;
					break;
				case 2:
					DisplayFileList(ELM_SD, "/PC98/ROMS");
					go = 0;
					break;
				case 3:
					// controller_config();
					break;
				case 4:
					EmulationOptions();
					break;
				case 5:
					ReturnTo(0);
					go = 0;
					break;
				case 6:
					ReturnTo(1);
					go = 0;
					break;
			}
			redraw = 1;
		}
		MENU_UP_DOWN_HANDLERS(listcnt)
		int hoff = HOFF;
		if(init) {
			PrintString(WOFF, hoff, "  Return to Emulation"); hoff += 16;
			PrintString(WOFF, hoff, "  Reset"); hoff += 16;
		}
		PrintString(WOFF, hoff, "  Switch Disks"); hoff += 16;
		PrintString(WOFF, hoff, "  Controller Config [DISABLED]"); hoff += 16;
		PrintString(WOFF, hoff, "  Emulation Options"); hoff += 16;
		PrintString(WOFF, hoff, "  Return to Loader"); hoff += 16;
		PrintString(WOFF, hoff, "  Return to Wii Menu"); hoff += 16;

		i = 0;
		// position == real  location
		// cur_off  == arrow location
		i = position - (MAX_ENTRIES - 1);
		if(i < 0)
			i = 0;
		if(i > (MAX_ENTRIES - 1)) {
			while(i > (MAX_ENTRIES - 1)) {
				i -= MAX_ENTRIES;
			}
		}
		int x;
		int bak = -1;
		hoff = HOFF;
		for(x = 0; (i < listcnt) && (x < MAX_ENTRIES); i++, x++) {
			if(position == i) {
				PrintString(WOFF, hoff, "*");
				bak = x;
			}
			hoff += 16;
		}
		redraw = 0;
		Video_Update();
		if(!wii_running_now)
			go = 0;
	}
}

void NP2Wii_menu_display()
{
	soundmng_stop();
	Video_PushScreen();
	Video_SetClear(1);
	Video_Update();
	DisplayMenu();
	Video_ClearScreen(0);
	if(wii_running_now) {
		Video_SetClear(0);
		Video_PopScreen();
		Video_Update();
		soundmng_play();
	}
	init = 1;
}

s16 NP2Wii_control_lookup(u32 k, int type)
{
	s16 out = -1;
	switch(k) {
		case WPAD_CLASSIC_BUTTON_UP:		/* 1B */
			out++;
		case WPAD_CLASSIC_BUTTON_LEFT:		/* 1A */
			out++;
		case WPAD_CLASSIC_BUTTON_ZR:		/* 19 */
			out++;
		case WPAD_CLASSIC_BUTTON_X:		/* 18 */
			out++;
		case WPAD_CLASSIC_BUTTON_A:		/* 17 */
			out++;
		case WPAD_CLASSIC_BUTTON_Y:		/* 16 */
			out++;
		case WPAD_CLASSIC_BUTTON_B:		/* 15 */
			out++;
		case WPAD_CLASSIC_BUTTON_ZL:		/* 14 */
			out++;
		case WPAD_CLASSIC_BUTTON_FULL_R:	/* 13 */
			out++;
		case WPAD_CLASSIC_BUTTON_PLUS:		/* 12 */
			out++;
		case WPAD_CLASSIC_BUTTON_HOME:		/* 11 */
			out++;
		case WPAD_CLASSIC_BUTTON_MINUS:		/* 10 */
			out++;
		case WPAD_CLASSIC_BUTTON_FULL_L:	/* 0F */
			out++;
		case WPAD_CLASSIC_BUTTON_DOWN:		/* 0E */
			out++;
		case WPAD_CLASSIC_BUTTON_RIGHT:		/* 0D */
			out++;
/*		case WPAD_NUNCHUK_BUTTON_Z:		   0C */
			out++;
/*		case WPAD_NUNCHUK_BUTTON_C:		   0B */
			out++;
		case WPAD_BUTTON_2:			/* 0A */
			out++;
		case WPAD_BUTTON_1:			/* 09 */
			out++;
		case WPAD_BUTTON_B:			/* 08 */
			out++;
		case WPAD_BUTTON_A:			/* 07 */
			out++;
		case WPAD_BUTTON_MINUS:			/* 06 */
			out++;
		case WPAD_BUTTON_HOME:			/* 05 */
			out++;
		case WPAD_BUTTON_PLUS:			/* 04 */
			out++;
		case WPAD_BUTTON_RIGHT:			/* 02 */
			out++;
		case WPAD_BUTTON_LEFT:			/* 03 */
			out++;
		case WPAD_BUTTON_UP:			/* 01 */
			out++;
		case WPAD_BUTTON_DOWN:			/* 00 */
			out++;
			break;
		default:
			out = 9001;
			break;
	}
	switch(type) {
		case EXP_NONE:
		case EXP_GUITAR_HERO_3:
		case EXP_WII_BOARD:
			type = 0;
			break;
		case EXP_NUNCHUK:
			type = 1;
			break;
		case EXP_CLASSIC:
			type = 2;
			break;
	}
	switch(out) {
		case 9001:
			return KEYUNUSED;
		default:
			if((type != 1) || ((out != 0x1B) && (out != 0x1A)))
				return wiimote_key_map[type][out];
			else
				return wiimote_key_map[type][out - 0x0F];
			break;
	}
}

void NP2Wii_control_handle()
{
	s16 z;
	struct expansion_t ext;
	WPAD_ScanPads();
	WPAD_Expansion(WPAD_CHAN_0, &ext);
	u32 btn = WPAD_ButtonsDown(WPAD_CHAN_0) | WPAD_ButtonsHeld(WPAD_CHAN_0);
	NP2WII_BTNCHK(WPAD_BUTTON_2)
	NP2WII_BTNCHK(WPAD_BUTTON_1)
	NP2WII_BTNCHK(WPAD_BUTTON_B)
	NP2WII_BTNCHK(WPAD_BUTTON_A)
	NP2WII_BTNCHK(WPAD_BUTTON_MINUS)
	NP2WII_BTNCHK(WPAD_BUTTON_HOME)
	NP2WII_BTNCHK(WPAD_BUTTON_PLUS)
	NP2WII_BTNCHK(WPAD_BUTTON_LEFT)
	NP2WII_BTNCHK(WPAD_BUTTON_RIGHT)
	NP2WII_BTNCHK(WPAD_BUTTON_UP)
	NP2WII_BTNCHK(WPAD_BUTTON_DOWN)
	if(ext.type == EXP_NUNCHUK) {
		NP2WII_BTNCHK(WPAD_NUNCHUK_BUTTON_Z)
		NP2WII_BTNCHK(WPAD_NUNCHUK_BUTTON_C)
	}
	if(ext.type == EXP_CLASSIC) {
		NP2WII_BTNCHK(WPAD_CLASSIC_BUTTON_UP)
		NP2WII_BTNCHK(WPAD_CLASSIC_BUTTON_LEFT)
		NP2WII_BTNCHK(WPAD_CLASSIC_BUTTON_RIGHT)
		NP2WII_BTNCHK(WPAD_CLASSIC_BUTTON_DOWN)
		NP2WII_BTNCHK(WPAD_CLASSIC_BUTTON_A)
		NP2WII_BTNCHK(WPAD_CLASSIC_BUTTON_B)
		NP2WII_BTNCHK(WPAD_CLASSIC_BUTTON_X)
		NP2WII_BTNCHK(WPAD_CLASSIC_BUTTON_Y)
		NP2WII_BTNCHK(WPAD_CLASSIC_BUTTON_ZR)
		NP2WII_BTNCHK(WPAD_CLASSIC_BUTTON_ZL)
		NP2WII_BTNCHK(WPAD_CLASSIC_BUTTON_FULL_R)
		NP2WII_BTNCHK(WPAD_CLASSIC_BUTTON_FULL_L)
		NP2WII_BTNCHK(WPAD_CLASSIC_BUTTON_PLUS)
		NP2WII_BTNCHK(WPAD_CLASSIC_BUTTON_HOME)
		NP2WII_BTNCHK(WPAD_CLASSIC_BUTTON_MINUS)
	}
}

static void NP2Wii_menu_init()
{
	if(X68000Printer_Init(file_getcd("x68kfont.rgb")) < 0) {
		exit(0);
	}
	usleep(100);
}

static void NP2Wii_menu_close()
{
	
}

static void NP2Wii_control_init()
{
	
}

static void NP2Wii_control_close()
{
	
}

void NP2Wii_init()
{
	PAD_Init();
	WPAD_Init();
	WPAD_SetPowerButtonCallback(NP2Wii_shutdown_wiimote);
	WPAD_SetDataFormat(WPAD_CHAN_ALL, WPAD_FMT_BTNS_ACC_IR);
	SYS_SetPowerCallback(NP2Wii_shutdown_power);
	SYS_SetResetCallback(NP2Wii_shutdown_power);
	oldbtn = 0;
	NP2Wii_menu_init();
	NP2Wii_control_init();
}


void NP2Wii_close()
{
	NP2Wii_menu_close();
	NP2Wii_control_close();
	NP2Wii_shutdown_power();
}

