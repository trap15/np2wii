/*****************************************************************************
 *  /wii/wii.h                                                               *
 *  Wii specific code header.                                                *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *
 *  Copyright (C)2009 SquidMan (Alex Marshall)       <SquidMan72@gmail.com>  *
 *  Protected under the GNU GPLv2                                            *
 *****************************************************************************/

#include <gctypes.h>

#if !defined(WII_H_)
#define WII_H_

void NP2Wii_init();
void NP2Wii_close();
void NP2Wii_menu_display();
void NP2Wii_shutdown_wiimote(s32 chan);
void NP2Wii_shutdown_power();
void NP2Wii_screenshot();
void NP2Wii_control_handle();

#endif /* !defined(WII_H_) */
