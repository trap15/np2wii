#include <stdio.h>
#include <stdlib.h>

#include "compiler.h"
#include "video.h"

#include <gccore.h>
#include <ogcsys.h>
#include <ogc/texconv.h>

static u32 SCRN_X;
static u32 SCRN_Y;
static u32 SCRN_BPP;

#define SCRNSTACKSIZE	16
u16* scrnstack[SCRNSTACKSIZE];
int scrnstackloc = 0;

static GXRModeObj *vmode = NULL;
static unsigned int *xfb[2] = { NULL, NULL };
static int fb = 0;
#define DEFAULT_FIFO_SIZE (256 * 1024)
static Mtx view;
static void  *gp_fifo = NULL;
static int quit_flip_thread = 0;
#define TEXTUREMEM_SIZE 	(640 * 480 * 4)
static u8 texturemem[TEXTUREMEM_SIZE] ATTRIBUTE_ALIGN(32);
static u8 textureconvert[TEXTUREMEM_SIZE] ATTRIBUTE_ALIGN(32);
static GXTexObj texobj;
static int video_locked = 1;
static int clearorigtex = 0;
/* New texture based scaler */
typedef struct tagcamera
{
	guVector pos;
	guVector up;
	guVector view;
}camera;

/* We'll use the PC98's aspect here, since it seems to stretch normally (?) */
#define HASPECT 			640
#define VASPECT 			400

/*** Square Matrix
 This structure controls the size of the image on the screen.
 Think of the output as a -80 x 80 by -60 x 60 graph.
 ***/
static s16 square[] ATTRIBUTE_ALIGN (32) =
{
	/*
	 *         X,            Y, Z
	 */
	-HASPECT / 2,  VASPECT / 2, 0,		// 0
	 HASPECT / 2,  VASPECT / 2, 0,		// 1
	 HASPECT / 2, -VASPECT / 2, 0,		// 2
	-HASPECT / 2, -VASPECT / 2, 0		// 3
};


static camera cam = {
	{ 0.0F,  0.0F,  0.0F},
	{ 0.0F,  0.5F,  0.0F},
	{ 0.0F,  0.0F, -0.5F}
};

int Video_Init(int width, int height)
{
	SCRN_X		= width;
	SCRN_Y		= height;
	SCRN_BPP	= 16;
	Video_SetClear(0);
	f32 yscale;
	u32 xfbHeight;
	Mtx44 perspective;
	s8 error_code = 0;
	int i;
	
	for(i = 0; i < SCRNSTACKSIZE; i++) {
		scrnstack[i] = calloc(SCRN_X * SCRN_Y, 2);
	}
	
	VIDEO_Init();
	vmode = VIDEO_GetPreferredMode(NULL);
	
	switch(vmode->viTVMode >> 2) {
		case VI_PAL: // 576 lines (PAL 50hz)
			// display should be centered vertically (borders)
			vmode = &TVPal574IntDfScale;
			vmode->xfbHeight = 480;
			vmode->viYOrigin = (VI_MAX_HEIGHT_PAL - 480)/2;
			vmode->viHeight = 480;
			break;
			
		case VI_NTSC: // 480 lines (NTSC 60hz)
			break;
			
		default: // 480 lines (PAL 60Hz)
			break;
	}

	VIDEO_Configure(vmode);
	xfb[0] = MEM_K0_TO_K1(SYS_AllocateFramebuffer(vmode));
	xfb[1] = MEM_K0_TO_K1(SYS_AllocateFramebuffer(vmode));
	
	VIDEO_ClearFrameBuffer(vmode, xfb[0], COLOR_BLACK);
	VIDEO_ClearFrameBuffer(vmode, xfb[1], COLOR_BLACK);

	VIDEO_SetNextFramebuffer(xfb[fb]);
	VIDEO_SetBlack(FALSE);
	VIDEO_Flush();
	VIDEO_WaitVSync();
	if (vmode->viTVMode & VI_NON_INTERLACE)
		VIDEO_WaitVSync();
	else
		while(VIDEO_GetNextField())
			VIDEO_WaitVSync();
	gp_fifo = (void*)memalign(32, DEFAULT_FIFO_SIZE);
	memset(gp_fifo, 0, DEFAULT_FIFO_SIZE);
	GX_Init(gp_fifo, DEFAULT_FIFO_SIZE);
	GX_SetCopyClear((GXColor){ 0, 0, 0, 0xFF }, GX_MAX_Z24);


	Mtx44 p;
	int df = 1; // deflicker on/off
	
	GX_SetViewport (0, 0, vmode->fbWidth, vmode->efbHeight, 0, 1);
	GX_SetDispCopyYScale((f32)vmode->xfbHeight / (f32)vmode->efbHeight);
	GX_SetScissor (0, 0, vmode->fbWidth, vmode->efbHeight);
	
	GX_SetDispCopySrc (0, 0, vmode->fbWidth, vmode->efbHeight);
	GX_SetDispCopyDst (vmode->fbWidth, vmode->xfbHeight);
	GX_SetCopyFilter (vmode->aa, vmode->sample_pattern, (df == 1) ? GX_TRUE : GX_FALSE, vmode->vfilter);
	
	GX_SetFieldMode (vmode->field_rendering, ((vmode->viHeight == 2 * vmode->xfbHeight) ? GX_ENABLE : GX_DISABLE));
	GX_SetPixelFmt (GX_PF_RGB8_Z24, GX_ZC_LINEAR);
	GX_SetDispCopyGamma (GX_GM_1_0);
	GX_SetCullMode (GX_CULL_NONE);
	GX_SetBlendMode(GX_BM_BLEND,GX_BL_DSTALPHA,GX_BL_INVSRCALPHA,GX_LO_CLEAR);
	
	GX_SetZMode (GX_TRUE, GX_LEQUAL, GX_TRUE);
	GX_SetColorUpdate (GX_TRUE);
	GX_SetNumChans(1);
	
	guOrtho(p, vmode->efbHeight / 2, -vmode->efbHeight / 2, -vmode->fbWidth / 2, vmode->fbWidth / 2, 100.0f, 1000.0f);
	GX_LoadProjectionMtx(p, GX_ORTHOGRAPHIC);
	
	GX_SetViewport(0, 0, vmode->fbWidth, vmode->efbHeight, 0, 1);
	GX_SetBlendMode(GX_BM_BLEND, GX_BL_SRCALPHA, GX_BL_INVSRCALPHA, GX_LO_CLEAR);
	GX_SetAlphaUpdate(GX_TRUE);
	GX_SetAlphaCompare(GX_GREATER, 0, GX_AOP_AND, GX_ALWAYS, 0);
	GX_SetColorUpdate(GX_ENABLE);
	GX_SetCullMode(GX_CULL_NONE);
	
	GX_ClearVtxDesc ();
	GX_SetVtxDesc (GX_VA_POS, GX_INDEX8);
	GX_SetVtxDesc (GX_VA_CLR0, GX_INDEX8);
	GX_SetVtxDesc (GX_VA_TEX0, GX_DIRECT);
	
	GX_SetVtxAttrFmt (GX_VTXFMT0, GX_VA_POS, GX_POS_XYZ, GX_S16, 0);
	GX_SetVtxAttrFmt (GX_VTXFMT0, GX_VA_CLR0, GX_CLR_RGBA, GX_RGBA8, 0);
	GX_SetVtxAttrFmt (GX_VTXFMT0, GX_VA_TEX0, GX_TEX_ST, GX_F32, 0);
	
	GX_SetArray (GX_VA_POS, square, 3 * sizeof (s16));
	
	GX_SetNumTexGens (1);
	GX_SetNumChans (0);
	
	GX_SetTexCoordGen (GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_IDENTITY);
	
	GX_SetTevOp (GX_TEVSTAGE0, GX_REPLACE);
	GX_SetTevOrder (GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP0, GX_COLORNULL);
	
	memset (&view, 0, sizeof (Mtx));
	guLookAt(view, &cam.pos, &cam.up, &cam.view);
	GX_LoadPosMtxImm (view, GX_PNMTX0);
	
	GX_InvVtxCache ();	// update vertex cache
	
	
	GX_InitTexObj(&texobj, texturemem, SCRN_X, SCRN_Y, GX_TF_RGB565, GX_CLAMP, GX_CLAMP, GX_FALSE);
	GX_LoadTexObj(&texobj, GX_TEXMAP0);	// load texture object so its ready to use
	
	VIDEO_SetBlack(0);
	video_locked = 0;
	return 0;
}

static void ConvertRGB565ToGX()
{
	int h, w;
	long long int *dst = (long long int *)texturemem;
	long long int *src1 = (long long int *)textureconvert;
	long long int *src2 = (long long int *)(textureconvert +  (SCRN_X * 2));
	long long int *src3 = (long long int *)(textureconvert + ((SCRN_X * 2) * 2));
	long long int *src4 = (long long int *)(textureconvert + ((SCRN_X * 2) * 3));
	int rowpitch = ((SCRN_X * 2) >> 3) * 3;
	int rowadjust = ((SCRN_X * 2) % 8) * 4;
	char *ra = NULL;
	
	for (h = 0; h < SCRN_Y; h += 4) {
		for (w = 0; w < SCRN_X; w += 4)	{
			*dst++ = *src1++;
			*dst++ = *src2++;
			*dst++ = *src3++;
			*dst++ = *src4++;
		}
		
		src1 += rowpitch;
		src2 += rowpitch;
		src3 += rowpitch;
		src4 += rowpitch;
		
		if ( rowadjust ) {
			ra = (char *)src1;
			src1 = (long long int *)(ra + rowadjust);
			ra = (char *)src2;
			src2 = (long long int *)(ra + rowadjust);
			ra = (char *)src3;
			src3 = (long long int *)(ra + rowadjust);
			ra = (char *)src4;
			src4 = (long long int *)(ra + rowadjust);
		}
	}
}

void Video_Update()
{
	if(!video_locked)		/* If we're locked, go ahead and update, but don't refresh the texture */
		ConvertRGB565ToGX();
	GX_InvVtxCache();
	GX_InvalidateTexAll();
	DCFlushRange(texturemem, TEXTUREMEM_SIZE);
	GX_LoadTexObj(&texobj, GX_TEXMAP0);
	Mtx m;
	Mtx mv;
	guMtxIdentity (m);
	guMtxTransApply (m, m, 0, 0, -100);
	guMtxConcat (view, m, mv);
	GX_LoadPosMtxImm (mv, GX_PNMTX0);
	GX_Begin (GX_QUADS, GX_VTXFMT0, 4);
		GX_Position1x8(0);
		GX_Color1x8(0);
		GX_TexCoord2f32(0.0, 0.0);
	
		GX_Position1x8(1);
		GX_Color1x8(0);
		GX_TexCoord2f32(1.0, 0.0);

		GX_Position1x8(2);
		GX_Color1x8(0);
		GX_TexCoord2f32(1.0, 1.0);

		GX_Position1x8(3);
		GX_Color1x8(0);
		GX_TexCoord2f32(0.0, 1.0);
	GX_End();
	GX_SetColorUpdate(GX_TRUE);
	fb ^= 1;
	GX_CopyDisp(xfb[fb], GX_TRUE);
	GX_DrawDone();
	if(clearorigtex)
		memset(textureconvert, 0, SCRN_X * SCRN_Y * 2);
	VIDEO_SetNextFramebuffer(xfb[fb]);
	VIDEO_Flush();
	VIDEO_WaitVSync();
}

void* Video_LockScreen()
{
	if(video_locked)
		return;
	video_locked = 1;
	return textureconvert;
}

void Video_UnlockScreen()
{
	video_locked = 0;
}

void Video_ClearScreen(u16 color)
{
	int i;
	u16* scrn = Video_LockScreen();
	if(color == 0)
		memset(scrn, 0, SCRN_X * SCRN_Y * 2);
	else{
		for(i = 0; i < (SCRN_X * SCRN_Y); i++) {
				scrn[i] = color;
		}
	}
	Video_UnlockScreen();
}

int Video_GetWidth()
{
	return SCRN_X;
}

int Video_GetHeight()
{
	return SCRN_Y;
}

void* Video_LoadImage(char* path)
{
	/* Loads .RGB files (created with IMG->RGB565) */
	FILE* fp = fopen(path, "rb");
	fseek(fp, 0, SEEK_END);
	int sz = ftell(fp);
	u8* data = malloc(sz);
	fseek(fp, 0, SEEK_SET);
	fread(data, sz, 1, fp);
	fclose(fp);
	return data;
}

void Video_BlitImage(void* img, int srcx, int srcy, int dstx, int dsty, int dstw, int dsth)
{
	u32* head = (u32*)img;
	int x, y;
	if(head[0] != 0x47580565) {
		/* Not a valid .RGB file. */
		exit(1);
	}
	if((dstw + srcx) > head[1]) {
		/* Too wide. */
		dstw = head[1] - srcx;
	}
	if((dstw + dstx) > SCRN_X) {
		/* Too wide. */
		dstw = SCRN_X - dstx;
	}
	if((dsth + srcy) > head[2]) {
		/* Too wide. */
		dsth = head[2] - srcy;
	}
	if((dsth + dsty) > SCRN_Y) {
		/* Too wide. */
		dsth = SCRN_Y - dsty;
	}
	u16* data = (u16*)(((u8*)head) + 12);
	data += srcx + (srcy * head[1]);
	u16* scrn = Video_LockScreen();
	scrn += dstx + (dsty * SCRN_X);
	for(x = 0; x < dstw; x++) {
		for(y = 0; y < dsth; y++) {
			if(data[x + (y * head[1])] != 0xF81B)
				scrn[x + (y * SCRN_X)] = data[x + (y * head[1])];
		}
	}
	Video_UnlockScreen();
}

void Video_SetClear(int on)
{
	clearorigtex = on;
}

void Video_PushScreen()
{
	u16* scrn = Video_LockScreen();
	if(scrnstackloc < SCRNSTACKSIZE)
		memcpy(scrnstack[scrnstackloc++], scrn, SCRN_X * SCRN_Y * 2);
	Video_UnlockScreen();
}

void Video_PopScreen()
{
	u16* scrn = Video_LockScreen();
	if(scrnstackloc > 0)
		memcpy(scrn, scrnstack[--scrnstackloc], SCRN_X * SCRN_Y * 2);
	Video_UnlockScreen();
}
