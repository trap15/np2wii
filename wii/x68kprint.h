/*****************************************************************************
 *  x68kprint.h                                                              *
 *  Some oldskool printing action!                                           *
 *  Copyright (c)2009 SquidMan (Alex Marshall)       <SquidMan72@gmail.com>  *
 *****************************************************************************/

#ifndef _X68KPRINT_H_
#define _X68KPRINT_H_

#define X68000Printer_PrintLocation(s, x, y)			\
		X68000Printer_SetX(0);				\
		X68000Printer_SetY(0);				\
		X68000Printer_SetXOff(x);			\
		X68000Printer_SetYOff(y);			\
		X68000Printer_Print(s)

int  X68000Printer_Init(char* font);
void X68000Printer_Print(char* string);
void X68000Printer_GetSize(char* string, int* w, int* h);
void X68000Printer_SetX(int x);
void X68000Printer_SetY(int y);
void X68000Printer_SetXOff(int x);
void X68000Printer_SetYOff(int y);
void X68000Printer_AddX(int x);
void X68000Printer_AddY(int y);
int  X68000Printer_GetWidth(char* string);
int  X68000Printer_GetHeight(char* string);

#endif /* _X68KPRINT_H_ */
