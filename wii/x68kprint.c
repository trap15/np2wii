/*****************************************************************************
 *  x68kprint.c                                                              *
 *  Some oldskool printing action!                                           *
 *  Copyright (c)2009 SquidMan (Alex Marshall)       <SquidMan72@gmail.com>  *
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "video.h"

typedef struct {
	signed long long int	x, y, w, h;
} box_t;

static int SCREEN_OFFSET_X = 0;
static int SCREEN_OFFSET_Y = 0;

static int tmpscroff_x = 0;
static int tmpscroff_y = 0;

static void* x68kfont;
static box_t prnt_loc = { 0, 0, 0, 0 };
static int prnt_mode = 0;
static int prnt_inc = 0;
static int prnt_init = 0;

static box_t font_boxs[] = { \
	{ 0,  0,  8, 16},	/* Standard English Set & Half-width Katakana */\
	{ 0,  0, 16, 16},	/* Full-width Katakana & Hiragana */		\
};

int X68000Printer_PrintCharCore(char* chs, signed long long int x, signed long long int y, int doprint)
{
	prnt_inc = 0;
	if(prnt_init == 0) {
		fprintf(stderr, "X68000 Printer is not initialized!\n");
		return 0;
	}
	char ch = chs[0];
	int chr = 0;
	switch(ch) {
		case '\x01':
			prnt_mode = 0;
			return 0;
		case '\x02':
			prnt_mode = 1;
			return 0;
		case '\x03':
			prnt_mode = 2;
			return 0;
		case '\x04':
			prnt_mode = 3;
			return 0;
	}
	switch(ch) {
		case '!':
			chr = (3 * 26) + 10;
			break;
		case '\"':
			chr = (3 * 26) + 11;
			break;
		case '#':
			chr = (3 * 26) + 12;
			break;
		case '$':
			chr = (3 * 26) + 13;
			break;
		case '%':
			chr = (3 * 26) + 14;
			break;
		case '&':
			chr = (3 * 26) + 15;
			break;
		case '\'':
			chr = (3 * 26) + 16;
			break;
		case '(':
			chr = (3 * 26) + 17;
			break;
		case ')':
			chr = (3 * 26) + 18;
			break;
		case '=':
			chr = (3 * 26) + 19;
			break;
		case '_':
			chr = (3 * 26) + 20;
			break;
		case '-':
			if(prnt_mode < 2)
				chr = (3 * 26) + 21;
			else
				chr = (14 * 26) + 25;
			break;
		case '@':
			chr = (3 * 26) + 22;
			break;
		case '^':
			chr = (3 * 26) + 23;
			break;
		case ',':
			if(prnt_mode == 0)
				chr = (3 * 26) + 24;
			else
				chr = (18 * 26) + 5;
			break;
		case '.':
			if(prnt_mode == 0)
				chr = (3 * 26) + 25;
			else
				chr = (18 * 26) + 6;
			break;
		case '/':
			chr = (4 * 26) + 0;
			break;
		case '<':
			chr = (4 * 26) + 1;
			break;
		case '>':
			chr = (4 * 26) + 2;
			break;
		case '?':
			chr = (4 * 26) + 3;
			break;
		case '+':
			chr = (4 * 26) + 4;
			break;
		case '*':
			if(prnt_mode == 0)
				chr = (4 * 26) + 5;
			else
				chr = (18 * 26) + 7;
			break;
		case '{':
			chr = (4 * 26) + 6;
			break;
		case '}':
			chr = (4 * 26) + 7;
			break;
		case ';':
			chr = (4 * 26) + 8;
			break;
		case ':':
			chr = (4 * 26) + 9;
			break;
		case '[':
			if(prnt_mode == 0)
				chr = (4 * 26) + 10;
			else
				chr = (18 * 26) + 8;
			break;
		case ']':
			if(prnt_mode == 0)
				chr = (4 * 26) + 11;
			else
				chr = (18 * 26) + 9;
			break;
		case '\\':
			chr = (4 * 26) + 12;
			break;
		case '|':
			chr = (4 * 26) + 13;
			break;
		case '\x05':
			chr = (4 * 26) + 14;
			break;
		case ' ':
			chr = (4 * 26) + 25;
			break;
	}
	if(chr != 0)
		goto prnt;
	
	if(prnt_mode == 0) {
		/* Latin charset */
		if((ch >= 'A') && (ch <= 'Z')) {
			chr = ch - 'A';
			chr += 26;
		}
		if(chr != 0)
			goto emoda;
		
		if((ch >= 'a') && (ch <= 'z')) {
			chr = ch - 'a';
			chr += 2 * 26;
		}
		if(chr != 0)
			goto emoda;
		
		if((ch >= '0') && (ch <= '9')) {
			chr = ch - '0';
			chr += 3 * 26;
		}
	emoda:
		if(chr == 0) {
			/* Panic, no character available! */
		}
#ifdef ENABLE_JAPANESE
	}else if(prnt_mode == 1) {
		/* Half-width Katakana charset */
		if(ch == '\x0F') {
			prnt_inc = 1;
			ch = chs[1];
		} else
			goto emodb;
		switch(ch >> 4) {
			case '\x01':	/* A */
				switch(ch & 0xF) {
					case '\x00':	/*   */
						chr = (6 * 26) + 2;
						break;
					case '\x01':	/* K */
						chr = (6 * 26) + 16;
						break;
					case '\x02':	/* S */
						chr = (7 * 26) + 11;
						break;
					case '\x03':	/* T */
						chr = (6 * 26) + 12;
						break;
					case '\x04':	/* N */
						chr = (6 * 26) + 18;
						break;
					case '\x05':	/* H */
						chr = (7 * 26) + 1;
						break;
					case '\x06':	/* M */
						chr = (7 * 26) + 4;
						break;
					case '\x07':	/* Y */
						chr = (6 * 26) + 6;
						break;
					case '\x08':	/* R */
						chr = (6 * 26) + 20;
						break;
					case '\x09':	/* W */
						chr = (6 * 26) + 9;
						break;
				}
				goto emodb;
				
			case '\x02':	/* I */
				switch(ch & 0xF) {
					case '\x00':	/*   */
						chr = (6 * 26) + 14;
						break;
					case '\x01':	/* K */
						chr = (7 * 26) + 2;
						break;
					case '\x02':	/* S (Shi) */
						chr = (7 * 26) + 0;
						break;
					case '\x03':	/* T (Chi) */
						chr = (6 * 26) + 24;
						break;
					case '\x04':	/* N */
						chr = (6 * 26) + 19;
						break;
					case '\x05':	/* H */
						chr = (7 * 26) + 13;
						break;
					case '\x06':	/* M */
						chr = (7 * 26) + 15;
						break;
					case '\x08':	/* R */
						chr = (7 * 26) + 6;
						break;
				}
				goto emodb;
				
			case '\x03':	/* U */
				switch(ch & 0xF) {
					case '\x00':	/*   */
						chr = (6 * 26) + 3;
						break;
					case '\x01':	/* K */
						chr = (7 * 26) + 3;
						break;
					case '\x02':	/* S */
						chr = (6 * 26) + 15;
						break;
					case '\x03':	/* T (Tsu) */
						chr = (7 * 26) + 10;
						break;
					case '\x04':	/* N */
						chr = (6 * 26) + 0;
						break;
					case '\x05':	/* H */
						chr = (6 * 26) + 1;
						break;
					case '\x06':	/* M */
						chr = (7 * 26) + 9;
						break;
					case '\x07':	/* Y */
						chr = (6 * 26) + 7;
						break;
					case '\x08':	/* R */
						chr = (6 * 26) + 18;
						break;
				}
				goto emodb;
				
			case '\x04':	/* E */
				switch(ch & 0xF) {
					case '\x00':	/*   */
						chr = (6 * 26) + 4;
						break;
					case '\x01':	/* K */
						chr = (7 * 26) + 8;
						break;
					case '\x02':	/* S */
						chr = (6 * 26) + 21;
						break;
					case '\x03':	/* T */
						chr = (6 * 26) + 13;
						break;
					case '\x04':	/* N */
						chr = (7 * 26) + 17;
						break;
					case '\x05':	/* H */
						chr = (6 * 26) + 11;
						break;
					case '\x06':	/* M */
						chr = (7 * 26) + 19;
						break;
					case '\x08':	/* R */
						chr = (7 * 26) + 7;
						break;
				}
				goto emodb;
				
			case '\x05':	/* O */
				switch(ch & 0xF) {
					case '\x00':	/*   */
						chr = (6 * 26) + 5;
						break;
					case '\x01':	/* K */
						chr = (7 * 26) + 14;
						break;
					case '\x02':	/* S */
						chr = (7 * 26) + 12;
						break;
					case '\x03':	/* T */
						chr = (6 * 26) + 25;
						break;
					case '\x04':	/* N */
						chr = (7 * 26) + 5;
						break;
					case '\x05':	/* H */
						chr = (6 * 26) + 10;
						break;
					case '\x06':	/* M */
						chr = (7 * 26) + 16;
						break;
					case '\x07':	/* Y */
						chr = (6 * 26) + 8;
						break;
					case '\x08':	/* R */
						chr = (7 * 26) + 20;
						break;
					case '\x09':	/* W */
						chr = (5 * 26) + 22;
						break;
				}
				goto emodb;
			case '\x06':	/* Special */
				switch(ch & 0xF) {
					case '\x00':	/* N */
						chr = (6 * 26) + 17;
						break;
					case '\x01':	/* Dakuten */
						chr = (6 * 26) + 22;
						break;
					case '\x02':	/* Handakuten */
						chr = (6 * 26) + 23;
						break;
					case '\x03':	/* Small a */
						chr = (7 * 26) + 21;
						break;
					case '\x04':	/* Small u */
						chr = (7 * 26) + 22;
						break;
					case '\x05':	/* Small e */
						chr = (7 * 26) + 23;
						break;
					case '\x06':	/* Small o */
						chr = (7 * 26) + 24;
						break;
					case '\x07':	/* Small ya */
						chr = (5 * 26) + 23;
						break;
					case '\x08':	/* Small yu */
						chr = (5 * 26) + 24;
						break;
					case '\x09':	/* Small yo */
						chr = (5 * 26) + 25;
						break;
					case '\x0A':	/* Small tsu */
						chr = (7 * 26) + 25;
						break;
				}
				goto emodb;
				
		}
	emodb:
		if(chr == 0) {
			/* Panic, no character available! */
		}
	}else if((prnt_mode == 2) || (prnt_mode == 3)) {
		/* Full-width Katakana charset (handles most of Hiragana too) */
		if(ch == '\x0F') {
			prnt_inc = 1;
			ch = chs[1];
		} else
			goto emodc;
		switch(ch >> 4) {
			case '\x01':	/* A */
				switch(ch & 0xF) {
					case '\x00':	/*   */
						chr = (8 * 26) + 2;
						break;
					case '\x01':	/* K */
						chr = (9 * 26) + 4;
						break;
					case '\x02':	/* S */
						chr = (10 * 26) + 12;
						break;
					case '\x03':	/* T */
						chr = (9 * 26) + 0;
						break;
					case '\x04':	/* N */
						chr = (9 * 26) + 6;
						break;
					case '\x05':	/* H */
						chr = (10 * 26) + 2;
						break;
					case '\x06':	/* M */
						chr = (10 * 26) + 5;
						break;
					case '\x07':	/* Y */
						chr = (8 * 26) + 6;
						break;
					case '\x08':	/* R */
						chr = (9 * 26) + 8;
						break;
					case '\x09':	/* W */
						chr = (8 * 26) + 9;
						break;
				}
				goto emodc;
				
			case '\x02':	/* I */
				switch(ch & 0xF) {
					case '\x00':	/*   */
						chr = (9 * 26) + 2;
						break;
					case '\x01':	/* K */
						chr = (10 * 26) + 3;
						break;
					case '\x02':	/* S (Shi) */
						chr = (10 * 26) + 1;
						break;
					case '\x03':	/* T (Chi) */
						chr = (9 * 26) + 12;
						break;
					case '\x04':	/* N */
						chr = (9 * 26) + 7;
						break;
					case '\x05':	/* H */
						chr = (11 * 26) + 1;
						break;
					case '\x06':	/* M */
						chr = (11 * 26) + 3;
						break;
					case '\x08':	/* R */
						chr = (10 * 26) + 7;
						break;
				}
				goto emodc;
				
			case '\x03':	/* U */
				switch(ch & 0xF) {
					case '\x00':	/*   */
						chr = (8 * 26) + 3;
						break;
					case '\x01':	/* K */
						chr = (10 * 26) + 4;
						break;
					case '\x02':	/* S */
						chr = (9 * 26) + 3;
						break;
					case '\x03':	/* T (Tsu) */
						chr = (10 * 26) + 11;
						break;
					case '\x04':	/* N */
						chr = (8 * 26) + 0;
						break;
					case '\x05':	/* H */
						chr = (8 * 26) + 1;
						break;
					case '\x06':	/* M */
						chr = (10 * 26) + 10;
						break;
					case '\x07':	/* Y */
						chr = (8 * 26) + 7;
						break;
					case '\x08':	/* R */
						chr = (11 * 26) + 6;
						break;
				}
				goto emodc;
				
			case '\x04':	/* E */
				switch(ch & 0xF) {
					case '\x00':	/*   */
						chr = (8 * 26) + 4;
						break;
					case '\x01':	/* K */
						chr = (10 * 26) + 9;
						break;
					case '\x02':	/* S */
						chr = (9 * 26) + 9;
						break;
					case '\x03':	/* T */
						chr = (9 * 26) + 1;
						break;
					case '\x04':	/* N */
						chr = (11 * 26) + 5;
						break;
					case '\x05':	/* H */
						chr = (6 * 26) + 11;
						break;
					case '\x06':	/* M */
						chr = (11 * 26) + 7;
						break;
					case '\x08':	/* R */
						chr = (10 * 26) + 8;
						break;
				}
				goto emodc;
				
			case '\x05':	/* O */
				switch(ch & 0xF) {
					case '\x00':	/*   */
						chr = (8 * 26) + 5;
						break;
					case '\x01':	/* K */
						chr = (11 * 26) + 2;
						break;
					case '\x02':	/* S */
						chr = (11 * 26) + 0;
						break;
					case '\x03':	/* T */
						chr = (10 * 26) + 0;
						break;
					case '\x04':	/* N */
						chr = (10 * 26) + 6;
						break;
					case '\x05':	/* H */
						chr = (8 * 26) + 10;
						break;
					case '\x06':	/* M */
						chr = (11 * 26) + 4;
						break;
					case '\x07':	/* Y */
						chr = (8 * 26) + 8;
						break;
					case '\x08':	/* R */
						chr = (11 * 26) + 8;
						break;
					case '\x09':	/* W */
						chr = (12 * 26) + 9;
						break;
				}
				goto emodc;
			case '\x06':	/* Special */
				switch(ch & 0xF) {
					case '\x00':	/* N */
						chr = (9 * 26) + 5;
						break;
					case '\x01':	/* Dakuten */
						chr = (6 * 26) + 22;
						break;
					case '\x02':	/* Handakuten */
						chr = (6 * 26) + 23;
						break;
					case '\x03':	/* Small a */
						chr = (11 * 26) + 9;
						break;
					case '\x04':	/* Small u */
						chr = (11 * 26) + 10;
						break;
					case '\x05':	/* Small e */
						chr = (11 * 26) + 11;
						break;
					case '\x06':	/* Small o */
						chr = (11 * 26) + 12;
						break;
					case '\x07':	/* Small ya */
						chr = (12 * 26) + 0;
						break;
					case '\x08':	/* Small yu */
						chr = (12 * 26) + 1;
						break;
					case '\x09':	/* Small yo */
						chr = (12 * 26) + 2;
						break;
					case '\x0A':	/* Small tsu */
						chr = (12 * 26) + 3;
						break;
				}
				goto emodc;
				
		}
	emodc:
		if(chr == 0) {
			/* Panic, no character available! */
		}
	}
	if(prnt_mode == 3) {
		chr += (6 * 26);
	}
#else
	}
#endif /* ENABLE_JAPANESE */
prnt:
	if(chr == 0) {
		return 0;
	}
	
	/* (finally) we do the printing. */
#ifdef ENABLE_JAPANESE
	box_t inloc;
	if(chr < (7 * 26))
		inloc = font_boxs[0];
	else
		inloc = font_boxs[1];
#else
	box_t inloc = font_boxs[0];
#endif /* ENABLE_JAPANESE */
	if(doprint == 1) {
		inloc.x = (chr % 26) * inloc.w;
		inloc.y = ((((chr - (chr % 26)) / 26) - 1) * inloc.h) + ((chr < (7 * 26)) ? 0 : 0);
		box_t loc;
		loc.x = x + SCREEN_OFFSET_X;
		loc.y = y + SCREEN_OFFSET_Y;
		Video_BlitImage(x68kfont, inloc.x, inloc.y, loc.x, loc.y, inloc.w, inloc.h);
	}
	return inloc.w;
}

int X68000Printer_PrintChar(char* chs, signed long long int x, signed long long int y)
{
	return X68000Printer_PrintCharCore(chs, x, y, 1);
}

int X68000Printer_GetCharWidth(char* chs)
{
	return X68000Printer_PrintCharCore(chs, 0, 0, 0);
}

void X68000Printer_GetSize(char* string, int* w, int* h)
{
	*w = X68000Printer_GetWidth (string);
	*h = X68000Printer_GetHeight(string);
}

void X68000Printer_Print(char* string)
{
	if(prnt_init == 0) {
		fprintf(stderr, "X68000 Printer is not initialized!\n");
		return;
	}
	int len = strlen(string);
	int i;
	for(i = 0; i < len; i++) {
		prnt_inc = 0;
		prnt_loc.x += X68000Printer_PrintChar(&(string[i]), prnt_loc.x + tmpscroff_x, prnt_loc.y + tmpscroff_y);
		switch(string[i]) {
			case '\t':
				prnt_loc.x += 8 - (prnt_loc.x % 8);
				break;
			case '\n':
				prnt_loc.x = 0;
				prnt_loc.y += 16;
				break;
		}
		if(prnt_loc.x >= (Video_GetWidth() - (SCREEN_OFFSET_X * 2))) {
			prnt_loc.x = 0;
			prnt_loc.y += 16;
		}
		if(prnt_loc.y >= (Video_GetHeight() - (SCREEN_OFFSET_Y * 2))) {
			prnt_loc.y = 0;
		}
		i += prnt_inc;
	}
	tmpscroff_x = 0;
	tmpscroff_y = 0;
}

int X68000Printer_GetWidth(char* string)
{
	int x;
	int i;
	int len;
	if(prnt_init == 0) {
		fprintf(stderr, "X68000 Printer is not initialized!\n");
		return 0;
	}
	len = strlen(string);
	x = 0;
	for(i = 0; i < len; i++) {
		prnt_inc = 0;
		char str[2] = {0, 0};
		str[0] = string[i];
		x += X68000Printer_GetCharWidth(str);
		i += prnt_inc;
	}
	return x;
}

int X68000Printer_GetHeight(char* string)
{
	int i;
	int y;
	int len;
	if(prnt_init == 0) {
		fprintf(stderr, "X68000 Printer is not initialized!\n");
		return 0;
	}
	len = strlen(string);
	y = 0;
	for(i = 0; i < len; i++) {
		switch(string[i]) {
			case '\n':
				y += 16;
				break;
		}
	}
	return y;
}

int X68000Printer_Init(char* font)
{
	x68kfont = Video_LoadImage(font);
	if(x68kfont == NULL) {
		fprintf(stderr, "Unable to load font\n");
		return -1;
	}
	prnt_init = 1;
	return 0;
}

void X68000Printer_SetX(int x)
{
	prnt_loc.x = x;
}

void X68000Printer_SetY(int y)
{
	prnt_loc.y = y;
}

void X68000Printer_SetXOff(int x)
{
	tmpscroff_x = x;
}

void X68000Printer_SetYOff(int y)
{
	tmpscroff_y = y;
}

void X68000Printer_AddX(int x)
{
	prnt_loc.x += x;
}

void X68000Printer_AddY(int y)
{
	prnt_loc.y += y;
}
